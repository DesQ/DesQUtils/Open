/**
 * Copyright 2012-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <iostream>
#include <QDir>

#include "Open.hpp"
#include <DFL/DF5/Xdg.hpp>
#include <desq/Utils.hpp>

DesQ::Open::Open() {
    mEnvPaths = qEnvironmentVariable( "PATH" ).split( ":" );

    mAppPaths << QDir::home().filePath( ".local/share/flatpak/exports/share/applications" );
    mAppPaths << QDir::home().filePath( ".local/share/applications" );
    mAppPaths << QDir::home().filePath( ".local/share/applications/wine" );
    mAppPaths << QDir::home().filePath( ".local/share/games" );
    mAppPaths << "/var/lib/flatpak/exports/share/applications";
    mAppPaths << "/usr/local/share/applications";
    mAppPaths << "/usr/local/share/games";
    mAppPaths << "/usr/share/applications";
    mAppPaths << "/usr/share/games";

    mWorkDir = QDir::homePath();
}


void DesQ::Open::setDesktopName( QString desktop ) {
    mApp  = desktop;
    isBin = false;
}


void DesQ::Open::setBinaryName( QString binary ) {
    mApp  = binary;
    isBin = true;
}


void DesQ::Open::setApplicationPaths( QStringList paths ) {
    if ( isBin ) {
        mEnvPaths = paths;
    }

    else {
        mAppPaths = paths;
    }
}


void DesQ::Open::setApplicationArgs( QStringList args ) {
    mAppArgs = args;
}


void DesQ::Open::setWorkingDirectory( QString wDir ) {
    mWorkDir = wDir;
}


void DesQ::Open::setFiles( QStringList files ) {
    mFiles = files;
}


bool DesQ::Open::launch() {
    /**
     * Procedure
     * 1. Obtain the desktop/binary file path
     * 2. Obtain the proxy information
     * 3. Obtain terminal information for binary apps
     */

    /** Prepare the proxy */
    DesQ::Environment proxyEnviron = DesQ::Utils::getNetworkProxy();

    QProcessEnvironment pe( QProcessEnvironment::systemEnvironment() );

    for ( QString key: proxyEnviron.keys() ) {
        pe.insert( key, proxyEnviron.value( key ) );
    }

    pe.insert( "PATH", mEnvPaths.join( ":" ) );
    pe.insert( "PWD",  mWorkDir );

    /** We're launching a binary. */
    if ( isBin ) {
        QProcess proc;
        proc.setProgram( mApp );
        proc.setArguments( mAppArgs + mFiles );
        proc.setWorkingDirectory( mWorkDir );
        proc.setProcessEnvironment( pe );

        return proc.startDetached();
    }

    /** We're launching an GUI */
    else {
        DFL::XDG::DesktopFile desktop( mApp );
        return desktop.startApplication( mAppArgs + mFiles, QString(), pe );
    }

    return false;
}
