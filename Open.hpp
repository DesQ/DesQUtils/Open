/**
 * Copyright 2012-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QString>
#include <QStringList>
#include <QDebug>

namespace DesQ {
    class Open;
}

class DesQ::Open {
    public:
        /** Init */
        Open();

        /** Name of the desktop */
        void setDesktopName( QString );

        /** Name of the binary */
        void setBinaryName( QString );

        /** Set desktop/binary search paths. Overrides the defaults */
        void setApplicationPaths( QStringList );

        /** Extra arguments to be passed to the app/binary */
        void setApplicationArgs( QStringList );

        /** Files to be opened */
        void setFiles( QStringList );

        /** Files to be opened */
        void setWorkingDirectory( QString );

        /** Launch the desktop/binary */
        bool launch();

    private:
        QStringList mEnvPaths;
        QStringList mAppPaths;
        QStringList mAppArgs;
        QStringList mFiles;

        QString mWorkDir;
        QString mApp;
        bool isBin;
};
