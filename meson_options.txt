option(
    'use_qt_version',
    type: 'combo',
    choices: ['auto', 'qt5', 'qt6'],
    value: 'auto',
    description: 'Select the Qt version to use'
)
