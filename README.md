# DesQ Open

A simple app to open various files and start various applications. DesQ Open inserts various environment variables (like proxy) to ensure the
apps have a uniform feel when opened from menu or command line. It is recommended to start all apps and files using the `desq-open` command.

### Options
```
Options:
  -h|--help               Displays help on commandline options.
  -v|--version            Displays version information.
  -a|--app <appname>      Start a desktop application.
  -b|--bin <binary>       Start a binary app
  -d|--appdirs <paths>    Paths to search for the application/binary, separated by `:`, and in the search order.
  -f|--files <filenames>  Open file(s). Separate files by `:`
  -g|--appargs <args>     Args to be passed to the application.
  -w|--workdir <workdir>  Start inside specified work directory.
  -p|--profile <profile>  Use a specific profile to start the app/file.
For feature requests, bug reports etc contact: <marcusbritanicus@gmail.com>
```

### Notes:
1. `<appname>` should be the name of the desktop file. In case you prefer to use the binary (f.e. lynx), provide the full path (i.e. /usr/bin/lynx).
2. `<appdirpath>` By default, the search order is
  - ~/.local/share/flatpak/exports/share/applications
  - ~/.local/share/applications
  - ~/.local/share/applications/wine
  - ~/.local/share/games
  - /var/lib/flatpak/exports/share/applications
  - /usr/local/share/applications
  - /usr/local/share/games
  - /usr/share/applications
  - /usr/share/games

### Examples:
- Open gui apps using desktop files. The Desktop file of pulsar is in ~/.local/share/applications and /usr/share/applications
    `desq-open --bin pulsar --appdirs ~/.local/share/applications:/usr/share/applications --workdir /tmp`

- Open two files, Open.cpp and Open.hpp in pulsar
    `desq-open --app pulsar --files Open.cpp:Open.hpp`
    ```
-
