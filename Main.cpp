/**
 * Copyright 2012-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <desq/Utils.hpp>
#include <DFL/DF5/CoreApplication.hpp>

#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <errno.h>
#include <sys/utsname.h>

#include "Open.hpp"

DFL::Settings *settings;

const char * platform() {
    struct utsname buffer;

    uname( &buffer );

    QString QtVersion  = QString( "Powered by Qt " ) + QT_VERSION_STR + " on Linux " + buffer.release;
    QString GCCVersion = QString( "Compiled with GCC " ) + __VERSION__;

    QString version = QtVersion + '\n' + GCCVersion;

    return qPrintable( version );
}


int main( int argc, char **argv ) {
    DFL::CoreApplication app( argc, argv );

    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "Open" );
    app.setApplicationVersion( PROJECT_VERSION );

    app.interceptSignal( SIGINT,  true );
    app.interceptSignal( SIGTERM, true );
    app.interceptSignal( SIGQUIT, true );
    app.interceptSignal( SIGABRT, true );
    app.interceptSignal( SIGSEGV, true );

    QCommandLineParser parser;

    parser.addOption( { { "h", "help" }, "Displays help on commandline options." } );
    parser.addOption( { { "v", "version" }, "Displays version information." } );
    parser.addOption( { { "a", "app" }, "Start a desktop application.", "appname" } );
    parser.addOption( { { "b", "binary" }, "Start a binary app.", "binary" } );
    parser.addOption( { { "d", "appdirs" }, "Paths to search for the application desktop, separated by colon.", "paths" } );
    parser.addOption( { { "f", "files" }, "Open one or more files. Separate multiple files with a semi-colon.", "filename" } );
    parser.addOption( { { "g", "appargs" }, "Args to be passed to the application.", "args" } );
    parser.addOption( { { "w", "workdir" }, "Start inside specified work directory.", "profile" } );
    parser.addOption( { { "p", "profile" }, "Use a specific profile to start the app/file.", "profile" } );

    /** Process the CLI arguments */
    parser.process( app );

    /** == Help and Version == **/
    if ( parser.isSet( "help" ) ) {
        std::cout << "DesQ Open " << PROJECT_VERSION << "\n" << std::endl;
        std::cout << "Usage: DesQ Open [options]" << std::endl;
        std::cout << "Options:" << std::endl;
        std::cout << "  -h|--help               Displays help on commandline options." << std::endl;
        std::cout << "  -v|--version            Displays version information." << std::endl;
        std::cout << "  -a|--app <appname>      Start a desktop application." << std::endl;
        std::cout << "  -b|--bin <binary>       Start a binary app" << std::endl;
        std::cout << "  -d|--appdirs <paths>    Paths to search for the application/binary, separated by `:`, and in the search order." << std::endl;
        std::cout << "  -f|--files <filenames>  Open file(s). Call multiple times for multiple files." << std::endl;
        std::cout << "  -g|--appargs <args>     Args to be passed to the application." << std::endl;
        std::cout << "  -w|--workdir <workdir>  Start inside specified work directory." << std::endl;
        std::cout << "  -p|--profile <profile>  Use a specific profile to start the app/file." << std::endl;
        std::cout << "For feature requests, bug reports etc contact: <marcusbritanicus@gmail.com>\n" << std::endl;

        return 0;
    }

    if ( parser.isSet( "version" ) ) {
        std::cout << "DesQ Open " << PROJECT_VERSION << std::endl;
        std::cout << platform() << std::endl;

        return 0;
    }

    if ( parser.isSet( "app" ) and parser.isSet( "binary" ) ) {
        std::cout << "You can specify only app or bin." << std::endl;

        return 0;
    }

    if ( not parser.isSet( "app" ) and not parser.isSet( "binary" ) and not parser.isSet( "files" ) ) {
        std::cout << "Specify either app or bin." << std::endl;
    }

    DesQ::Open opener;

    if ( parser.isSet( "binary" ) ) {
        opener.setBinaryName( parser.value( "binary" ) );
    }

    else {
        opener.setDesktopName( parser.value( "app" ) );
    }

    if ( parser.isSet( "appdirs" ) ) {
        QStringList dirs = parser.value( "appdirs" ).split( ":" );
        opener.setApplicationPaths( dirs );
    }

    if ( parser.isSet( "workdir" ) ) {
        opener.setWorkingDirectory( parser.value( "workdir" ) );
    }

    if ( parser.isSet( "files" ) ) {
        QStringList files = parser.value( "files" ).split( ":" );
        opener.setFiles( files );
    }

    opener.launch();

    return 0;
}
